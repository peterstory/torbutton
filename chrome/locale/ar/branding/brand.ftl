# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = متصفح تور
-brand-short-name = متصفح تور
-brand-full-name = متصفح تور
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = متصفح Tor
-vendor-short-name = مشروع تور
trademarkInfo = 'تور' و 'شعار البصلة' علامات تجارية مسجلة باسم شركة مشروع تور المحدودة
