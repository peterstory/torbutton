<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "À propos de Tor ">

<!ENTITY aboutTor.viewChangelog.label "Visualiser le journal des changements">

<!ENTITY aboutTor.ready.label "Explorez, en toute confidentialité.">
<!ENTITY aboutTor.ready2.label "Vous êtes prêt pour l’expérience de navigation la plus confidentielle au monde.">
<!ENTITY aboutTor.failure.label "Un problème est survenu !">
<!ENTITY aboutTor.failure2.label "Tor ne fonctionne pas dans ce navigateur.">

<!ENTITY aboutTor.search.label "Chercher avec DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Des questions ?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Consultez notre guide d’utilisation du Navigateur Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "G">
<!ENTITY aboutTor.torbrowser_user_manual.label "Guide d’utilisation du Navigateur Tor">

<!ENTITY aboutTor.tor_mission.label "Le Projet Tor est un organisme sans but lucratif US 501(c)(3) qui fait progresser les droits de la personne et les libertés en créant et en déployant des technologies gratuites d’anonymat et de protection de la vie privée et des données personnelles, à code source ouvert. Nous soutenons leur disponibilité et leur utilisation sans restriction, et promouvons une meilleure compréhension scientifique et populaire.">
<!ENTITY aboutTor.getInvolved.label "Impliquez-vous »">

<!ENTITY aboutTor.newsletter.tagline "Obtenez les dernières nouvelles au sujet de Tor directement dans votre boîte de réception.">
<!ENTITY aboutTor.newsletter.link_text "Inscrivez-vous aux nouvelles de Tor">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor peut être utilisé gratuitement grâce aux dons de personnes telles que vous.">
<!ENTITY aboutTor.donationBanner.buttonA "Faites un don maintenant">

<!-- End of year 2020 Fundraising campaign -->
<!ENTITY aboutTor.ey2020.useamask "Utilisez un masque, utilisez Tor.">
<!ENTITY aboutTor.ey2020.resistsurveillance "Résistez à la pandémie de surveillance.">
<!ENTITY aboutTor.ey2020.donationmatch "Votre don sera égalé par les Amis de Tor.">
