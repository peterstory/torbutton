<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Despre Tor">

<!ENTITY aboutTor.viewChangelog.label "Vezi Schimbări">

<!ENTITY aboutTor.ready.label "Explorează. În mod privat.">
<!ENTITY aboutTor.ready2.label "Ești gata pentru cel mai sigur mod de navigare din întreaga lume.">
<!ENTITY aboutTor.failure.label "Ceva a mers prost!">
<!ENTITY aboutTor.failure2.label "Tor nu funcționează în acest browser.">

<!ENTITY aboutTor.search.label "Caută cu DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Întrebări?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Verifică manualul Tor Browser »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manualul Tor Browser">

<!ENTITY aboutTor.tor_mission.label "Tor Project este o organizație non-profit US 501(c)(3) ce promovează drepturile și libertățile omului prin crearea și implementarea de tehnologii gratuite și open source pentru anonimitate și confidențialitate, sprijinind disponibilitatea și utilizarea lor nerestricționată și continuând întelegerea lor științifică și populară.">
<!ENTITY aboutTor.getInvolved.label "Implică-te »">

<!ENTITY aboutTor.newsletter.tagline "Obțineți ultimele știri de la Tor direct în căsuța de e-mail.">
<!ENTITY aboutTor.newsletter.link_text "Abonează-te la Tor News.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor poate fi folosit gratuit datorită donațiilor de la oameni ca tine.">
<!ENTITY aboutTor.donationBanner.buttonA "Donează Acum">

<!-- End of year 2020 Fundraising campaign -->
<!ENTITY aboutTor.ey2020.useamask "Folosește o mască, folosește Tor.">
<!ENTITY aboutTor.ey2020.resistsurveillance "Rezistă pandemiei de supraveghere.">
<!ENTITY aboutTor.ey2020.donationmatch "Your donation will be matched by Friends of Tor.">
